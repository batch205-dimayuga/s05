class Cart {
    constructor(){
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product,qty){
      this.contents.push({
        product: product,
        quantity: qty
      })
      this.computeTotal();
      return this;
    };

    showCartContents(){
        console.log(this.contents)
        return this;
    };

    clearCartContents(){
        this.contents = [];
        this.totalAmount = 0;
        return this;
    };

    computeTotal(){
        let sum = 0;
        let index = 0;
        this.contents.forEach(product => {
            sum += (this.contents[index].product.price * this.contents[index].quantity)
            index++;
        })

        this.totalAmount = sum;
        return this;
    }
}



class Product {
    constructor(prodName,price){

        if(typeof prodName !== 'string'){
			this.name = undefined
		} else{
			this.name = prodName;
		};

        
        if(typeof price !== 'number'){
			this.price = undefined
		} else{
			this.price = price;
		};

        this.isActive = true;
    }

    archive(){
        this.isActive = false;
        return this;

    };

    updatePrice(newPrice){

        if(typeof newPrice !== 'number'){
            console.log("Price should be a number.")
        } else {
            this.price = newPrice
        }
        return this;
    };

    checkPrice(prod){
        return prod.price;
    }
}

let cart1 = new Cart();
let prodA = new Product("Mechanical Keyboard", 5000);
let prodB = new Product("Bluetooth Speakers", 3000);
// cart1.addToCart(prodA,3)
// cart1.addToCart(prodB,2)
console.log(cart1)


class Customer {
    constructor(email){
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut(){
        if(this.cart.length !== 0){
            this.orders.push({
                products: this.cart.contents,
                totalAmount: this.cart.computeTotal().totalAmount
            })
        } else {
            console.log("Cart is Empty.")
        }
        return this;
    }
}

let john = new Customer('john@mail.com');